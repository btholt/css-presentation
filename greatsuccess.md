# Great suCeSS!
## Cultural Learnings of Cascading Style Sheet for Make Benefit Glorious Empire of Needle

---

# More about Brian

- Love to rockclimb
- Favorite movie: Gladiator
- Favorite book: One of Malcom Gladwell's books
- A total lightweight when it comes to alcohol
- Tried unsuccessfully to start a non-profit
- Huge Utah Jazz. Large RSL and Seattle Seahawks fan

---

# Favorite bands

- M83
- The Joy Formidable
- St Lucia
- Lana Del Rey
- Faded Paper Figures
- Mates of State
- School of Seven Bells
- Metric
- Purity Ring
- MS MR
- Delta Spirit

---

# CSS
## Cascading Style Sheets
![CSS Not Programming](../img/cssnotprogramming.jpeg)

---

# What is CSS?

- Front-end development is three things: content, style, and behavior.
- CSS is the style.
- The lines can begin to blur, particularly between [style and behavior](http://www.romancortes.com/ficheros/css-coke.html).
- CSS itself in not a programming language. It's a set of rules of how different elements on a page are styled.

---

## What is a selector?

- A selector is the statement that is used to apply rules to elements.
- Example: the selector "div" will select all divs on the page.
- The way your structure is important! It can drastically affect the performance and reusability of your code.

---

## Selector Syntax!

	* {
		box-sizing: border-box;
	}
	
	a {
		text-decoration: none;
		color: #AAA;
		font-size: large;
	}

---

## Selector Syntax!

	.cool-class {
		position: relative;
		top: -10px;
		left: 10px;
	}
	
	#primary-id {
		display: none;
	}

---

## Class vs. ID: When to use which

 - There may only be one instance of an ID on a page; there may be many of a class.
 - Favor using classes over ID when in doubt. A class may be used multiple times on a page and thus is more useful. By using an ID, you are limiting yourself to ever using it on that one ID.
 - Use meaningful names; use structural or purposeful names over presentational. 
 - Don't use .yellow-box, .right-panel, or .circle-picture.
 - Do use .info-well, .nav-panel, or .article-image.

---

## [What's the difference between the two?](../img/ggg.jpg)

	div .info

	div.info

---

## The first means a .info classed object inside of div. Example:
	&lt;div&gt;
		&lt;a class=&quot;info&quot; href=&quot;cool.html&quot;&gt;Link&lt;/a&gt;
	&lt;/div&gt;

---

## The second would select only divs with that class. Example:
	&lt;div class=&quot;info&quot;&gt;
		Cool story bro.
	&lt;/div&gt;

---

## How should I make my CSS selectors?

- Rarely you should need more than three selectors. ".page-container #stream .stream-item .tweet .tweet-header .username" isn't necessary. If it is, redo your HTML.
- Avoid using generic tags like div, a, or strong unless you intend on doing it for the whole page or at least for that section.

---

## Demo: Avoid generics

---

![Great Success!](../img/greatsuccess.jpg)

---

## Specificity
- Specificity refers to how the browser decides which CSS rule takes precedence.
- It's based on a scoring system. It's intuitive enough that most of the time people devs never learn precisely how it works.

---

## Stolen from CSS-Tricks.com
![CSS Specificityy](../img/specificity-calculationbase.png)

---

## Specificity

- This can pretty much be read like a number. If you have an ID in your selector, no many how many classes your have in your selector, that ID will take precedence.
- Remember this is only to really resolve conflicts. If there is no conflicts, then all the styling from all the different rules will apply.

---

- ![CSS Calculation](../img/cssspecificity-calc-1.png)
- ![CSS Calculation](../img/cssspecificity-calc-2.png)

---

- ![CSS Calculation](../img/cssspecificity-calc-4.png)
- ![CSS Calculation](../img/cssspecificity-calc-5.png)

---

## Notes
- Universal selector (*) has no specificity value.
- Pseudo-elements (e.g. :first-line) have the same specificity value as an element. This is different than pseudo-classes (e.g. :hover) which have the same as a class.
- :not() does not itself have a specificity value, but whatever is in its parenthesis does.
- CSS values marked !important always "win." That said, it's a bit of a crutch and shouldn't be used without good reason.

---

## Demo: Specificity

---

![High Five!](../img/highfive.jpg)

---

![Specificity Wars](../img/specificitywars.jpg)

---